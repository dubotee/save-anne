﻿using UnityEngine;
using System.Collections;

public class TurnoffRender : MonoBehaviour {

	// Use this for initialization

	void Start () {
		GetComponent<Renderer>().enabled = false;	
	}

}
