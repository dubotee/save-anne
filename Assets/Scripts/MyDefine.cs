﻿using UnityEngine;
using System.Collections;

public enum eObjectType
{
    Capsule = 0,
    Cube,
    Sphere,
    Poly,
    Cylinder,
    SpecialObject,
}

public enum GameState
{
    StartGame,
    Ingame,
    End,
}

public enum eCollectionState
{
    Freedom,
    Stand,
    Hold,
    AutoDead,
	HalfDead,
    Goal,
}

public enum eGoalState
{
    CreateRequest,
    Compare,
	Goaling,
    Goal,
}

public enum eControllerState
{
    Freedom,
    Hold,
	Dying,
	Dead,
}

public enum eMyEffectState
{
    Run,
    Dead
}

