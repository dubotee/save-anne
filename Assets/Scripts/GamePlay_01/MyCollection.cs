﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

public class MyCollection : MonoBehaviour
{
	PlaygroundParticlesC particles;

    public eObjectType type;
    public float minSpeed; 
    public float maxSpeed;
    float speed;

    public int score;
    public int subScore;

    public float minRot;
    public float maxRot;
    float rot;
    public eCollectionState currentState;
    public float timeLife;
    float counter, halfDeadCounter;
    public float yDetectArenaProtect;

    // Update is called once per frame
    void Update()
    {
        if (GoalManager.Instance == null)
        {
            return;
        }
        switch (currentState)
        {
            case eCollectionState.Freedom:
                //reset
                RemoveParticlePG();
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                //

                transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
                if (GoalManager.Instance.GetPositionRelative(transform.position + new Vector3(0, yDetectArenaProtect, 0)) == -1)
                {
                    transform.Translate(Vector3.left * speed * 0.5f * Time.deltaTime, Space.World);
                }
                else if (GoalManager.Instance.GetPositionRelative(transform.position + new Vector3(0, yDetectArenaProtect, 0)) == 1)
                {
                    transform.Translate(Vector3.right * speed * 0.5f * Time.deltaTime, Space.World);
                }
                transform.RotateAroundLocal(Vector3.one, rot * Time.deltaTime);

                transform.position = new Vector3(transform.position.x, transform.position.y, GoalManager.Instance.gameObject.transform.position.z);
                if (transform.position.y > GameManager.endY)
                {
                    currentState = eCollectionState.AutoDead;
                }
                break;

            case eCollectionState.Stand:
                transform.RotateAroundLocal(Vector3.one, rot * Time.deltaTime);
                break;

            case eCollectionState.Hold:
                counter += Time.deltaTime;

                //Set emissive color
                Color updatedEmissiveColor = new Color();
                updatedEmissiveColor.r = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").r, 0, 0.75f + counter / timeLife / 4);
                updatedEmissiveColor.g = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").g, 0, 0.75f + counter / timeLife / 4);
                updatedEmissiveColor.b = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").b, 0, 0.75f + counter / timeLife / 4);
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", updatedEmissiveColor);

                AddParticlePG();

                if (counter >= timeLife)
                {
                    GameManager.Instance.SetSelectedCollection(null);
                    currentState = eCollectionState.AutoDead;
                }
                transform.RotateAroundLocal(Vector3.one, rot * Time.deltaTime);
                transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * 10);
                break;
            case eCollectionState.HalfDead:
                halfDeadCounter += Time.deltaTime;

                //Set emissive color
                updatedEmissiveColor = new Color();
                updatedEmissiveColor.r = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").r, 0, 0.75f + halfDeadCounter / 4);
                updatedEmissiveColor.g = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").g, 0, 0.75f + halfDeadCounter / 4);
                updatedEmissiveColor.b = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").b, 0, 0.75f + halfDeadCounter / 4);
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", updatedEmissiveColor);

                //new particle playground
                if (halfDeadCounter >= 1.0)
                {
                    GameManager.Instance.SetSelectedCollection(null);
                    currentState = eCollectionState.AutoDead;
                }
                break;

            case eCollectionState.AutoDead:
                RemoveParticlePG();
                GameManager.Instance.Despawn(this);
                break;

            case eCollectionState.Goal:
                break;

            default:
                break;
        }
    }

    public virtual void Init()
	{
		halfDeadCounter = 0.0f;
        speed = Random.Range(minSpeed, maxSpeed);
        rot = Random.Range(minRot, maxRot);
        GetComponent<Collider>().isTrigger = false;
        currentState = eCollectionState.Freedom;
		Destroy(particles);
    }

    private Vector3 screenPoint;
    Vector3 target;
    void OnMouseDown()
    {
        if (currentState != eCollectionState.Stand &&
            currentState != eCollectionState.Goal)
        {
            currentState = eCollectionState.Hold;
            counter = 0;
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            target = Camera.main.ScreenToWorldPoint(screenPoint);
			target = new Vector3(target.x, target.y, GoalManager.Instance.gameObject.transform.position.z);
            GetComponent<Collider>().isTrigger = true;
            GameManager.Instance.SetSelectedCollection(this);
        }
    }

    void OnMouseDrag()
    {
        if (currentState != eCollectionState.Stand &&
            currentState != eCollectionState.Goal)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            target = Camera.main.ScreenToWorldPoint(curScreenPoint);
			target = new Vector3(target.x, target.y, GoalManager.Instance.gameObject.transform.position.z);
        }
    }

    void OnMouseUp()
    {
        if (currentState != eCollectionState.Stand &&
		    currentState != eCollectionState.Goal&&
		    currentState != eCollectionState.HalfDead)
        {
            currentState = eCollectionState.Freedom;
            counter = 0;
            GetComponent<Collider>().isTrigger = false;
            GameManager.Instance.SetSelectedCollection(null);
        }
    }

    public void SetStand()
    {
        currentState = eCollectionState.Stand;
    }

    public bool IsHold()
    {
        return currentState == eCollectionState.Hold;
    }

    void OnTriggerEnter(Collider col)
    {
		if (currentState != eCollectionState.Stand && currentState != eCollectionState.HalfDead)
        {
            MyCollection coll = col.GetComponent<MyCollection>();
            if (coll != null && coll.IsHold())
            {
                GameManager.Instance.SetSelectedCollection(null);
                coll.currentState = eCollectionState.HalfDead;
				currentState = eCollectionState.HalfDead;

				//Manipulator 
				coll.AddManipulatorPG(this.gameObject);
            }
        }
    }

	public void AddParticlePG()
	{
		if (particles == null)
		{
			particles = PlaygroundC.Particle();
			particles.gameObject.layer = LayerMask.NameToLayer("Playable");
			particles.source = SOURCEC.WorldObject;
			particles.lifetime = 1;
			particles.worldObject.gameObject = this.gameObject;
			particles.clearParticlesOnEmissionStop = true;
			particles.worldObjectUpdateNormals = true;
			particles.worldObjectUpdateVertices = true;
			particles.initialLocalVelocityMin = new Vector3(-1, -1, -1);
			particles.initialLocalVelocityMax = new Vector3(1, 1, 1);
			particles.gravity = new Vector3(4, -6, 0);
		}
	}

	public void AddManipulatorPG(GameObject target)
	{
		AddParticlePG();

		ManipulatorObjectC targetMani = new ManipulatorObjectC();
		particles.manipulators.Add(targetMani);
		
		targetMani.type = MANIPULATORTYPEC.Property;
		targetMani.transform.SetFromTransform(target.transform);
		
		targetMani.property.type = MANIPULATORPROPERTYTYPEC.MeshTarget;
		targetMani.property.meshTarget.gameObject = target;
		
		targetMani.property.targetSorting = TARGETSORTINGC.Scrambled;
		targetMani.property.transition = MANIPULATORPROPERTYTRANSITIONC.Lerp;
		targetMani.shape = MANIPULATORSHAPEC.Sphere;
		targetMani.size = 10.0f;
		targetMani.strength = 10.0f;
	}

	public void RemoveParticlePG()
	{
		if (particles != null)
		{
			particles.loop = false;
			particles.disableOnDone = true;
			particles.disableOnDoneRoutine = ONDONE.Destroy;
		}
	}
}
