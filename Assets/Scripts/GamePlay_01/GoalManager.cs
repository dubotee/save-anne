﻿using UnityEngine;
using System.Collections;

public class GoalManager : Singleton<GoalManager> {

    public MyCollection requestCollection;
    public Transform capsule;
    public Transform cube;
    public Transform sphere;
    public Transform poly;
    public Transform cylinder;

    public PoolManager pollManager;
    public eObjectType requestObjectType;
    public eGoalState currentState;
    public float dicCollect;
    public float arenaProtect;
	float halfDeadCounter;

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, dicCollect);
        Gizmos.DrawWireSphere(transform.position, arenaProtect);
    }

	// Use this for initialization
	void Start () {
        currentState = eGoalState.CreateRequest;
        currentCountObj = countObj;
	}
	
    int currentCountObj;
    int countObj;
	// Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case eGoalState.CreateRequest:
                currentCountObj++;
                if (currentCountObj > countObj)
                {
                    int i = Random.Range(0, 3);
                    switch (i)
                    {
                        case 0:
                            requestObjectType = eObjectType.Capsule;
                            break;
                        case 1:
                            requestObjectType = eObjectType.Sphere;
                            break;
                        case 2:
                            requestObjectType = eObjectType.Cube;
                            break;
                        default:
                            break;
                    }
                    countObj = Random.Range(3, 6);
                    currentCountObj = 0;
                }
                Vector3 pos = new Vector3(0, 0, 0);
                MyCollection temp = null;
                switch (requestObjectType)
                {
                    case eObjectType.Capsule:
                        temp = pollManager.Spawn(capsule, pos).GetComponent<MyCollection>();
                        break;
                    case eObjectType.Cube:
                        temp = pollManager.Spawn(cube, pos).GetComponent<MyCollection>();
                        break;
                    case eObjectType.Sphere:
                        temp = pollManager.Spawn(sphere, pos).GetComponent<MyCollection>();
                        break;
                    case eObjectType.Poly:
                        temp = pollManager.Spawn(poly, pos).GetComponent<MyCollection>();
                        break;
                    case eObjectType.Cylinder:
                        temp = pollManager.Spawn(cylinder, pos).GetComponent<MyCollection>();
                        break;
                    default:
                        break;
                }
                if (temp != null)
                {
                    temp.Init();
                    temp.SetStand();
                    if (temp.GetComponent<Rigidbody>() != null)
                    {
                        temp.GetComponent<Rigidbody>().isKinematic = true;
                    }
                    requestCollection = temp;
                }

				halfDeadCounter = 0.0f;
				//Set emissive color
				Color updatedEmissiveColor = new Color();
				updatedEmissiveColor.r = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").r, 0, 0.99f);
				updatedEmissiveColor.g = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").g, 0, 0.99f);
				updatedEmissiveColor.b = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").b, 0, 0.99f);
				requestCollection.GetComponent<Renderer>().material.SetColor("_EmissionColor", updatedEmissiveColor);

                currentState = eGoalState.Compare;
                break;
            case eGoalState.Compare:
                if (GameManager.Instance != null && GameManager.Instance.selectedCollection != null)
                {
                    Vector3 dic = GameManager.Instance.selectedCollection.transform.position - requestCollection.transform.position;
                    if (dic.magnitude <= dicCollect)
                    {
                        if ((GameManager.Instance.selectedCollection.type == requestCollection.type || GameManager.Instance.selectedCollection.type == eObjectType.SpecialObject))
                        {
                            GameManager.Instance.Goal();
                            currentState = eGoalState.Goaling;
                        }
                        else
                        {
                            GameManager.Instance.SubGoal();
                            currentState = eGoalState.Goaling;
                        }
                    }
                }
                break;
			case eGoalState.Goaling:
				halfDeadCounter += Time.deltaTime;
				
				//Set emissive color
				updatedEmissiveColor = new Color();
				updatedEmissiveColor.r = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").r, 0, 0.75f + halfDeadCounter/4);
				updatedEmissiveColor.g = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").g, 0, 0.75f + halfDeadCounter/4);
				updatedEmissiveColor.b = Mathf.Lerp(requestCollection.GetComponent<Renderer>().material.GetColor("_DiffuseColor").b, 0, 0.75f + halfDeadCounter/4);
				requestCollection.GetComponent<Renderer>().material.SetColor("_EmissionColor", updatedEmissiveColor);
				
				//new particle playground
				if (halfDeadCounter >= 1.2)
				{
					currentState = eGoalState.Goal;
				}
				else if (halfDeadCounter >= 1)
				{
					requestCollection.gameObject.SetActive(false);
				}
				break;
            case eGoalState.Goal:
                pollManager.Despawn(requestCollection.transform);
                currentState = eGoalState.CreateRequest;
                break;
            default:
                break;
        }
    }

    public int GetPositionRelative(Vector3 input)
    {
        Vector3 temp = transform.position - input;
        if (temp.magnitude <= arenaProtect)
        {
            if (transform.position.x < input.x)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return 0;
        }
    }
}
