﻿using UnityEngine;
using System.Collections;

public class MyPoly : MyCollection {

    public override void Init()
    {
        Color invColor = new Color();
        invColor.r = Random.Range(0.0f, 1.0f);
        invColor.g = Random.Range(0.0f, 1.0f);
        invColor.b = Random.Range(0.0f, 1.0f);
		this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", invColor);

        type = eObjectType.Poly;
        base.Init();
    }
}
