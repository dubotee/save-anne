﻿using UnityEngine;
using System.Collections;

public class MyStar : MyCollection {

    public Color color;
    public override void Init()
    {
        this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", color);

        type = eObjectType.SpecialObject;
        base.Init();
    }
}
