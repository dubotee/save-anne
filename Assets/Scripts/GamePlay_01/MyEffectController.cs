﻿using UnityEngine;
using System.Collections;

public class MyEffectController : MonoBehaviour {

    public Vector3 startScale;
    public Vector3 endScale;
    public float timeAction;
    float counter;

    public Transform dummy;
    public eMyEffectState currentState;

	// Update is called once per frame
	void Update () {
        switch (currentState)
        {
            case eMyEffectState.Run:
                counter += Time.deltaTime;
                transform.localScale = Vector3.Lerp(startScale, endScale, counter / timeAction);
                float radius = (transform.position - dummy.position).magnitude;
                GameManager.Instance.DeadInRadius(radius);
                if (counter >= timeAction)
                {
                    currentState = eMyEffectState.Dead;
                }
                break;
            case eMyEffectState.Dead:
                gameObject.SetActiveRecursively(false);
                break;
            default:
                break;
        }
	}

    void OnEnable()
    {
        counter = 0;
        transform.localScale = startScale;
        currentState = eMyEffectState.Run;
    }
}
