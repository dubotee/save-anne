﻿using UnityEngine;
using System.Collections;

public class MySphere : MyCollection {

    public Color color;
    public override void Init()
    {
        this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", color);

        type = eObjectType.Sphere;
        base.Init();
    }
}
