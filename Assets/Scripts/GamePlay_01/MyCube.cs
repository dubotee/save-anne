﻿using UnityEngine;
using System.Collections;

public class MyCube : MyCollection {

    public Color color;
    public override void Init()
    {
        this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", color);

        type = eObjectType.Cube;
        base.Init();
    }
}
