﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
    public Transform capsule;
    public Transform cube;
    public Transform sphere;
    public Transform poly;
    public Transform cylinder;
    public Transform special;

    public List<float> weightRandom;
    float sumWeight;
    public float timeSpawnObj;

    public PoolManager pollManager;
    public float score;
    public MyCollection selectedCollection;
    public MyEffectController myEffect;

    float counter;

    public List<MyCollection> myCollections;

    public GameState currentState;

    public float minX;
    public float maxX;
    public float startY;
    public static float endY = 6;

    public int maxScore;
    public int yellowScore;
    public int blueScore;
    public int redScore;
    public float deltaScoreDown;
    public float timePlay;
    float countTimePlay;

    public bool isInitScoreBar;

    // Use this for initialization
    void Start()
    {
        myCollections = new List<MyCollection>();
        foreach (var item in weightRandom)
        {
            sumWeight += item;
        }
        currentState = GameState.StartGame;
        score = 0;
        countTimePlay = timePlay;
        isInitScoreBar = false;

        InitScoreBar();
    }

    public void StartGame()
    {
        currentState = GameState.StartGame;
        score = 0;
        countTimePlay = timePlay;
        UpdateScore();
    }

    public void InitScoreBar()
    {
        if (GuiManager.Instance.ingame1Gui != null)
        {
            GuiManager.Instance.ingame1Gui.SetYellowBlueRedWidthMySlider(yellowScore / (float)maxScore, blueScore / (float)maxScore);
            isInitScoreBar = true;
        }
    }

    void OnValidate()
    {
        redScore = maxScore - yellowScore - blueScore;
    }
    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case GameState.StartGame:
                score = 0;
                currentState = GameState.Ingame;
                break;
            case GameState.Ingame:
                UpdateTime();
                UpdateScore();
                counter += Time.deltaTime;
                if (counter >= timeSpawnObj)
                {
                    Vector3 pos = new Vector3(Random.Range(minX, maxX), startY, 0);
                    float random = Random.Range(0, sumWeight);
                    float count = 0;
                    for (int i = 0; i < weightRandom.Count; i++)
                    {
                        count += weightRandom[i];
                        if (random <= count)
                        {
                            switch (i)
                            {
                                case 0:
                                    MyCollection temp = pollManager.Spawn(capsule, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 1:
                                    temp = pollManager.Spawn(cube, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 2:
                                    temp = pollManager.Spawn(sphere, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 3:
                                    temp = pollManager.Spawn(poly, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 4:
                                    temp = pollManager.Spawn(cylinder, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 5:
                                    temp = pollManager.Spawn(special, pos).GetComponent<MyCollection>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    }
                    counter = 0;
                }
                break;
            case GameState.End:
                break;
            default:
                break;
        }
    }

    public void UpdateTime()
    {
        countTimePlay -= Time.deltaTime;
        if (countTimePlay > 0 )
        {
            GuiManager.Instance.ingame1Gui.SetTime((int)countTimePlay);
        }
        else
        {
            if (MenuMng.instance)
            {
                if (score <= yellowScore)
                {
                    //Thua Yellow
                    MenuMng.instance.SetLoseScore(((int)score).ToString(), false);
                    MenuMng.instance.setState((int)MenuDef.menuState.LOSEGAME);
                }
                else if(score <= yellowScore + blueScore)
                {
                    //Thang
                    MenuMng.instance.SetWinScore(((int)score).ToString());
                    MenuMng.instance.setState((int)MenuDef.menuState.WINGAME);
                }
                else
                {
                    //Thua Red
                    MenuMng.instance.SetLoseScore(((int)score).ToString(), true);
                    MenuMng.instance.setState((int)MenuDef.menuState.LOSEGAME);
                }
                
                currentState = GameState.End;
            }
        }
    }

    public void UpdateScore()
    {
        if (score >= 0 && score <= maxScore)
        {
            score -= deltaScoreDown * Time.deltaTime;
            GuiManager.Instance.ingame1Gui.SetMySliderValue(score / maxScore, (int) score);
        }
    }

    public void Despawn(MyCollection myCollection)
    {
        pollManager.Despawn(myCollection.transform);
        myCollections.Remove(myCollection);
    }

    public void SetSelectedCollection(MyCollection coll)
    {
        selectedCollection = coll;
    }

    public void Goal()
    {
        score += selectedCollection.score;
        if (score > maxScore)
        {
            score = maxScore;
        }

        if (selectedCollection.type == eObjectType.SpecialObject)
        {
            myEffect.gameObject.SetActiveRecursively(true);
        }
		selectedCollection.AddManipulatorPG (GoalManager.Instance.requestCollection.gameObject);
        selectedCollection.currentState = eCollectionState.HalfDead;
        SetSelectedCollection(null);

        SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_GETPOINT);
    }

    public void SubGoal()
    {
        score -= selectedCollection.subScore;
        if (score < 0)
        {
            score = 0;
        }

        if (selectedCollection.type == eObjectType.SpecialObject)
        {
            myEffect.gameObject.SetActiveRecursively(true);
        }
        selectedCollection.AddManipulatorPG(GoalManager.Instance.requestCollection.gameObject);
        selectedCollection.currentState = eCollectionState.HalfDead;
        SetSelectedCollection(null);

        SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_WRONGHIT);
    }

    public void DeadInRadius(float radius)
    {
        foreach (var item in myCollections)
        {
            if ((item.transform.position - myEffect.transform.position).magnitude <= radius &&
                item.type != GoalManager.Instance.requestObjectType)
            {
				Instantiate (Resources.Load("PlaygroundExplosive", typeof(GameObject)), item.transform.position, Quaternion.identity);
				item.currentState = eCollectionState.AutoDead;
            }
        }
    }
}
