﻿using UnityEngine;
using System.Collections;

public class BG_GlowingOrbitingCube : MonoBehaviour
{
	float pulseSpeed;
	float phase;
	//float randomval = Random.Range(

	Vector3 Vec3(float x)
	{
		return new Vector3(x, x, x);
	}

	void Start() 
	{
		transform.localScale = Vec3(1.5f);
		pulseSpeed = Random.Range(1.0f, 3.0f);
		phase = Random.Range(0.0f, Mathf.PI * 4.0f);
	}
	
	void Update() 
	{
		Color color = BG_Global.mainCube.glowColor;
		color.r = 1.0f - color.r;
		color.g = 1.0f - color.g;
		color.b = 1.0f - color.b;
		color = Color.Lerp(color, Color.white, 0.1f);


		color *= Mathf.Pow(Mathf.Sin(Time.time * pulseSpeed + phase) * 0.49f + 0.21f, 2.0f);


		GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
		GetComponent<Light>().color = color;
	}
}
