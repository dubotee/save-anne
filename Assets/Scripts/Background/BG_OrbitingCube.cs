﻿using UnityEngine;
using System.Collections;

public class BG_OrbitingCube : MonoBehaviour
{

	Transform transf;

	Vector3 rotationVector;
	float rotationSpeed;

	Vector3 spherePosition;

	Vector3 randomSphereRotation;

	float sphereRotationSpeed;
	float offsetY = 0.0f;

	Vector3 Vec3(float x)
	{
		return new Vector3(x, x, x);
	}

	void Start() 
	{
		transf = this.transform;
		rotationVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		rotationVector = Vector3.Normalize(rotationVector);

		spherePosition = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		spherePosition = Vector3.Normalize(spherePosition);
		spherePosition *= Random.Range(16.5f, 108.0f);
		//spherePosition *= 20.0f;

		randomSphereRotation = new Vector3(Random.Range(-1.1f, 1.0f), Random.Range(0f, 0.1f), Random.Range(0.5f, 1f));
		randomSphereRotation = Vector3.Normalize(randomSphereRotation);

		sphereRotationSpeed = Random.Range(10f, 30f);

		rotationSpeed = Random.Range(-90f, 90f);

		transf.localScale = Vec3(Random.Range(1.0f, 4.0f));

		Color invColor = new Color();
		invColor.r = Random.Range (0.0f,  1.0f);
		invColor.g = Random.Range (0.0f,  1.0f);
		invColor.b = Random.Range (0.0f,  1.0f);
		
		this.GetComponent<Renderer>().material.SetColor("_Color", invColor);
		offsetY = Random.Range (-200.0f, -100.0f);
	}
	void Update() 
	{
		offsetY += Time.deltaTime * 15;
		offsetY = transf.position.y > 100.0f ? Random.Range(-200.0f, -100.0f) : offsetY;
		Quaternion sphereRotation = Quaternion.Euler(randomSphereRotation * Time.time * sphereRotationSpeed);
		Vector3 pos = sphereRotation * spherePosition;
		//pos += spherePosition * (Mathf.Sin(Time.time - spherePosition.magnitude / 10.0f) * 0.5f + 0.5f);
		//pos += sphereRotation * spherePosition * (Mathf.Sin((Time.time * 3.1415265f / 4.0f) - spherePosition.magnitude / 10.0f) * 0.5f + 0.5f);
		transf.position = pos + new Vector3(0, offsetY, 0);
		transf.rotation = Quaternion.Euler(rotationVector * Time.time * rotationSpeed);
	}
}
