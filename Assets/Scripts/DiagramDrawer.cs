﻿using UnityEngine;
using System.Collections;

public class DiagramDrawer : MonoBehaviour {
	// Creates a line renderer that follos a Sin() function
	// and animates it.
	Color c1  = Color.white;
	Color c2 = Color.white;
	int lengthOfLineRenderer = 27;
	float stepWidth = 0.2f;
	float [] scoreSegments;
	void Start() {
		scoreSegments = new float[12];
		for(int i = 0; i < scoreSegments.Length; i++) {
			scoreSegments[i] = Random.Range(0.5f, 2f)* stepWidth;
		}

		LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material (Shader.Find("Particles/Additive"));
		lineRenderer.SetColors(c1, c2);
		lineRenderer.SetWidth(0.1f,0.1f);
		lineRenderer.useWorldSpace = false;
		lineRenderer.SetVertexCount(lengthOfLineRenderer);
	}
	void Update() {
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		for(int i = 0; i < lengthOfLineRenderer; i++) {
			lineRenderer.SetPosition(i, new Vector3( (i-lengthOfLineRenderer/2) * stepWidth, 0, 0));
		}

		int scoreSegIndex = 0;
		for (int i = 2; i < lengthOfLineRenderer-2; i+=2)
		{
			lineRenderer.SetPosition(i, new Vector3( (i-lengthOfLineRenderer/2) * stepWidth, scoreSegments[scoreSegIndex], 0));
			scoreSegIndex++;
		}
	}
}
