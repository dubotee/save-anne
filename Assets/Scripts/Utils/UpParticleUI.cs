﻿using UnityEngine;
using System.Collections;

public class UpParticleUI : MonoBehaviour {

    public int renderQueue = 10000;

    Material mMat;

    void Start()
    {
        SkeletonAnimation myAnim1 = GetComponent<SkeletonAnimation>();
        Renderer ren = myAnim1.GetComponent<Renderer>();
        ren.material.renderQueue = renderQueue;
        if (ren == null)
        {
            MeshRenderer myAnim = GetComponent<MeshRenderer>();
            Debug.Log("------UpParticleUI: ");

            GetComponent<Renderer>().material.renderQueue = renderQueue;
            if (myAnim1 != null)
            {
                Debug.Log("----UpParticleUI: " + myAnim1.GetComponent<Renderer>().name);
                //myAnim1.renderer.material.renderQueue = 4000;
            }
            //Debug.Log("----UpParticleUI: " + myAnim.materials[0].shader.name);
            //if (myAnim != null) ren = myAnim.materials[0].shader.name;
            //ParticleSystem sys = GetComponent<ParticleSystem>();
            //if (sys != null) ren = sys.renderer;
        }

        //if (ren != null)
        //{
        //    Debug.Log("----UpParticleUI??: ");
        //    mMat = new Material(ren.sharedMaterial);
        //    mMat.renderQueue = renderQueue;
        //    ren.material = mMat;
        //}
    }

    void OnDestroy() { if (mMat != null) Destroy(mMat); }
}
