﻿using UnityEngine;
using System.Collections;

public class AnimBasic : MonoBehaviour{
    //Moving
    public bool MoveX;
    public bool MoveY;
    public bool MoveZ;

    public float speedMove;
    public float MinMove;
    public float MaxMove;
    //private Vector3 originalPos;
    private int moveDir;
    private float MaxM;
    //Rotate
    public bool isRotateAround;
    public Vector3 Rotate;
    //private Vector3 originalRotate;
    public float minRotate;
    public float MaxRotate;
    private float MaxR;
    private int rotateDir;


    //Scale
    public bool isScaling;
    public float minScale = 0.2f;
    public float maxScale = 1.2f;
    public float perScale;
    public int speedScale = 10;
    private bool isLimitScale = false;
    private float isChangeScale = 1.0f;

    void Start()
    {
        //Move
        //originalPos = transform.position;
        moveDir = 1;

        //Rotate
        //originalRotate = transform.rotation.eulerAngles;
        rotateDir = 1;
        if (minRotate > 0)
            minRotate *= -1;

        //Scale
    }

    void FixedUpdate()
    {
        UpdateMove();
        UpdateRotate();
        UpdateScale();
    }

    void UpdateMove()
    {
        MaxMove = (MaxMove < 0.01) ? (float)0.01 : MaxMove;
        MinMove = (MinMove > -0.01) ? (float)-0.01 : MinMove;

        if (MaxM >= MaxMove)
        {
            moveDir = -1;
        }
        if (MaxM <= MinMove)
        {
            moveDir = 1;
        }
        MaxM = MaxM + moveDir * speedMove * Time.deltaTime;

        float x = 0.0f, y = 0.0f, z = 0.0f;
        speedMove = (speedMove < 1) ? 1 : speedMove;
        if (MoveX)
            x = moveDir * speedMove * Time.deltaTime;
        if (MoveY)
            y = moveDir * speedMove * Time.deltaTime;
        if (MoveZ)
            z = moveDir * speedMove * Time.deltaTime;

        Vector3 tmps = new Vector3(x, y, z);
        transform.position = transform.position + tmps;
    }

    void UpdateRotate()
    {
        //Rotating
        Vector3 vRotate = new Vector3(0.0f, 0.0f, 0.0f);
        vRotate.x = (Rotate.x > 0) ? 1.0f : 0.0f;
        vRotate.y = (Rotate.y > 0) ? 1.0f : 0.0f;
        vRotate.z = (Rotate.z > 0) ? 1.0f : 0.0f;
        float speedRotate = Rotate.x + Rotate.y + Rotate.z;
        //speedRotate = (speedRotate <= 1) ? 1 : speedRotate;

        minRotate = (minRotate > -1) ? -1 : minRotate;
        MaxRotate = (MaxRotate < 1) ? 1 : MaxRotate;
        //MaxR += speedRotate * Time.deltaTime;
        //if (MaxRotate > minRotate)
        {
            if (MaxR > MaxRotate * 4 / 5 || MaxR > minRotate * 4 / 5 && MaxR < 0)
            {
                //if (MaxR >= 0)
                //    speedRotate = speedRotate / (MaxR / MaxRotate * 10 / 2);
                //else
                //    speedRotate = speedRotate / (MaxR / minRotate * 10 / 2);
                //speedRotate /= 3;
                //speedRotate = (speedRotate <= 1) ? 1 : speedRotate;
                //Utils.Log("---speedRotate: " + speedRotate);
            }

            if (MaxR >= MaxRotate)
            {
                rotateDir = -1;
                //Utils.Log("---1 set rotateDir: " + rotateDir);
            }
            if (MaxR <= minRotate)
            {
                rotateDir = 1;
                //Utils.Log("---2 set rotateDir: " + rotateDir);
            }
            //else
            //if (rotateDir == -1)
            {
                //Utils.Log("---MaxR: " + MaxR);
                //Utils.Log("---rotateDir: " + rotateDir);
                //Utils.Log("---MaxR+: " + (speedRotate * Time.deltaTime * rotateDir));
                MaxR = MaxR + speedRotate * Time.deltaTime * rotateDir;
            }
        }
        if (isRotateAround)
        {
            if (speedRotate < 0)
            {
                speedRotate *= -1.0f;
                transform.RotateAroundLocal(-vRotate, speedRotate * Time.deltaTime);
            }
            else
            {
                transform.RotateAroundLocal(vRotate, speedRotate * Time.deltaTime);
            }
        }
        else
            transform.Rotate(vRotate * speedRotate * Time.deltaTime * rotateDir);
        ////Utils.Log("Rotate: " + transform.rotation.eulerAngles);
    }

    void UpdateScale()
    {
        //Scaling
        Vector3 vScale = new Vector3(0.1f, 0.1f, 0.1f);
        // perScale /= 100;
        if (isScaling)
        {
            isLimitScale = ((transform.localScale.x < minScale || transform.localScale.y < minScale || transform.localScale.z < minScale) ||
                            (transform.localScale.x > maxScale || transform.localScale.y > maxScale || transform.localScale.z > maxScale) ?
                            true : false);
            if (isLimitScale)
            {
                isLimitScale = false;
                isChangeScale *= -1.0f;
            }
            speedScale = (speedScale <= 1) ? 1 : speedScale;
            transform.localScale -= vScale * isChangeScale * Time.deltaTime * speedScale;
        }
    }
}
