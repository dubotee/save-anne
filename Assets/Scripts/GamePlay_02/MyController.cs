﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;
using System.Collections.Generic;

public class MyController : MonoBehaviour {

    public List<Transform> mains;
    public List<Color> colorMains;
    int index;
    public eControllerState currentState;
    public float speed;
    public eObjectType requestType;
	public Transform aura;
	public Transform auraCamera;
	public PlaygroundParticlesC particles;

	float rot;
	public float minRot;
	public float maxRot;
	float startHoldTime;

	// Use this for initialization
	void Start () {
        //Color invColor = new Color();
        //invColor.r = Random.Range(0.0f, 1.0f);
        //invColor.g = Random.Range(0.0f, 1.0f);
        //invColor.b = Random.Range(0.0f, 1.0f);
        //this.renderer.material.SetColor("_DiffuseColor", invColor);
        StartGame();
		rot = Random.Range(minRot, maxRot);
	}

    public void StartGame()
    {
        index  = Random.Range(0, 3);
        for (int i = 0; i < mains.Count; i++)
        {
            if (i == index)
            {
                mains[i].gameObject.SetActiveRecursively(true);
            }
            else
            {
                mains[i].gameObject.SetActiveRecursively(false);
            }
        }
        switch (index)
        {
            case 0:
                requestType = eObjectType.Capsule;
                break;
            case 1:
                requestType = eObjectType.Cylinder;
                break;
            case 2:
                requestType = eObjectType.Poly;
                break;
            default:
                break;
        }
        mains[index].GetComponent<Renderer>().material.SetColor("_DiffuseColor", colorMains[index]);
    }
	
	// Update is called once per frame
	void Update () {
		particles.emissionRate = Vector3.Magnitude (target - this.transform.position) * 2;
		aura.position = (transform.position - auraCamera.position) * 1.2f + auraCamera.position;
		transform.RotateAroundLocal(Vector3.one, rot * Time.deltaTime);
        switch (currentState)
        {
            case eControllerState.Freedom:
				mains[index].GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color (0, 0, 0));
                break;
            case eControllerState.Hold:
                //transform.LookAt(target);
                transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * speed);

                Color color = mains[index].GetComponent<Renderer>().material.GetColor("_EmissionColor");
				color.r = Mathf.Pow(Mathf.Clamp((Mathf.Sin(Time.time - startHoldTime) + 1) * 0.1f + 0.2f, 0, 1 ), 3.0f);
				color.g = Mathf.Pow(Mathf.Clamp((Mathf.Sin(Time.time - startHoldTime) + 1) * 0.1f + 0.2f, 0, 1 ), 3.0f);
				color.b = Mathf.Pow(Mathf.Clamp((Mathf.Sin(Time.time - startHoldTime) + 1) * 0.1f + 0.2f, 0, 1 ), 3.0f);
                mains[index].GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
                break;
			case eControllerState.Dying:
				aura.GetComponent<Renderer>().material.SetFloat("_Brightness", Mathf.Clamp01(aura.GetComponent<Renderer>().material.GetFloat("_Brightness") - 3 * Time.deltaTime));
				if (aura.GetComponent<Renderer>().material.GetFloat("_Brightness") <= 0.1f)
					currentState = eControllerState.Dead;
				break;
			case eControllerState.Dead:
				aura.gameObject.SetActive(false);
				particles.loop = false;
				this.gameObject.SetActiveRecursively(false);
				Game2Manager.Instance.currentState = GameState.End;
				break;
            default:
                break;
        }
	}

    private Vector3 screenPoint;
    Vector3 target;
    void OnMouseDown()
    {
		if(currentState != eControllerState.Dying && currentState != eControllerState.Dead)
        {
            currentState = eControllerState.Hold;
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            target = Camera.main.ScreenToWorldPoint(screenPoint);
			target = new Vector3(target.x, target.y, transform.parent.position.z);
			startHoldTime = Time.time;
        }
    }

    void OnMouseDrag()
    {
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            target = Camera.main.ScreenToWorldPoint(curScreenPoint);
			target = new Vector3(target.x, target.y, transform.parent.position.z);
        }
    }

    void OnMouseUp()
    {
		if(currentState != eControllerState.Dying && currentState != eControllerState.Dead)
		{
			startHoldTime = 0.0f;
    		currentState = eControllerState.Freedom;
		}
    }
}
