﻿using UnityEngine;
using System.Collections;

public class MyPoly2 : MyCollection2
{
    public Color color;
    public override void Init()
    {
        this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", color);

        type = eObjectType.Poly;
        base.Init();
    }
}