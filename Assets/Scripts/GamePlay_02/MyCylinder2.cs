﻿using UnityEngine;
using System.Collections;

public class MyCylinder2 : MyCollection2
{
    public Color color;
    public override void Init()
    {
        this.GetComponent<Renderer>().material.SetColor("_DiffuseColor", color);

        type = eObjectType.Cylinder;
        base.Init();
    }
}