﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

public class MyCollection2 : MonoBehaviour
{
	PlaygroundParticlesC particles;

    public eObjectType type;
    public float minSpeed;
    public float maxSpeed;
    float speed;
    public int score;
    public int subScore;
    public float minRot;
    public float maxRot;
    float rot;
    public eCollectionState currentState;
    public float timeLife;
    float counter;
    public float yDetectArenaProtect;
	float halfDeadCounter;

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case eCollectionState.Freedom:
                transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
                transform.RotateAroundLocal(Vector3.one, rot * Time.deltaTime);
				transform.position = new Vector3(transform.position.x, transform.position.y, transform.parent.position.z);
                if (transform.position.y > Game2Manager.endY)
                {
                    currentState = eCollectionState.AutoDead;
                }
                break;

            case eCollectionState.AutoDead:
			RemoveParticlePG();
                Game2Manager.Instance.Despawn(this);
                break;

            case eCollectionState.HalfDead:
				halfDeadCounter += Time.deltaTime;
				
				//Set emissive color
				Color updatedEmissiveColor = new Color();
				updatedEmissiveColor.r = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").r, 0, 0.75f + halfDeadCounter/4);
				updatedEmissiveColor.g = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").g, 0, 0.75f + halfDeadCounter/4);
				updatedEmissiveColor.b = Mathf.Lerp(this.GetComponent<Renderer>().material.GetColor("_DiffuseColor").b, 0, 0.75f + halfDeadCounter/4);
				this.GetComponent<Renderer>().material.SetColor("_EmissionColor", updatedEmissiveColor);
				
				//new particle playground
				if (halfDeadCounter >= 1.0)
				{
					//Game2Manager.Instance.SetSelectedCollection(null);
					currentState = eCollectionState.AutoDead;
				}
				break;
            default:
                break;
        }
    }

    public virtual void Init()
    {
		halfDeadCounter = 0.0f;
        speed = Random.Range(minSpeed, maxSpeed);
        rot = Random.Range(minRot, maxRot);
        GetComponent<Collider>().isTrigger = false;
        currentState = eCollectionState.Freedom;
    }

    void OnTriggerEnter(Collider col)
    {
        MyController controller = col.GetComponent<MyController>();
        if (controller != null)
        {
			if (controller.currentState != eControllerState.Dead && controller.currentState != eControllerState.Dying)
			{
	            if (type == controller.requestType)
	            {
					AddManipulatorPG(Game2Manager.Instance.controller.gameObject);
					currentState = eCollectionState.HalfDead;
	                Game2Manager.Instance.Collected(this);
	            }
	            else
	            {
                    AddManipulatorPG(Game2Manager.Instance.controller.gameObject);
                    currentState = eCollectionState.HalfDead;
                    Game2Manager.Instance.SubCollected(this);
	            }
			}
        }
    }
	
	
	public void AddParticlePG()
	{
		if (particles == null)
		{
			particles = PlaygroundC.Particle();
			particles.gameObject.layer = LayerMask.NameToLayer("Playable");
			particles.source = SOURCEC.WorldObject;
			particles.lifetime = 1;
			particles.worldObject.gameObject = this.gameObject;
			particles.clearParticlesOnEmissionStop = true;
			particles.worldObjectUpdateNormals = true;
			particles.worldObjectUpdateVertices = true;
			particles.initialLocalVelocityMin = new Vector3(-1, -1, -1);
			particles.initialLocalVelocityMax = new Vector3(1, 1, 1);
		}
	}
	
	public void AddManipulatorPG(GameObject target)
	{
		AddParticlePG();
		
		ManipulatorObjectC targetMani = new ManipulatorObjectC();
		particles.manipulators.Add(targetMani);
		
		targetMani.type = MANIPULATORTYPEC.Property;
		targetMani.transform.SetFromTransform(target.transform);
		
		targetMani.property.type = MANIPULATORPROPERTYTYPEC.MeshTarget;
		targetMani.property.meshTarget.gameObject = target;
		
		targetMani.property.targetSorting = TARGETSORTINGC.Scrambled;
		targetMani.property.transition = MANIPULATORPROPERTYTRANSITIONC.Lerp;
		targetMani.shape = MANIPULATORSHAPEC.Sphere;
		targetMani.size = 10.0f;
		targetMani.strength = 10.0f;
	}
	
	public void RemoveParticlePG()
	{
		if (particles != null)
		{
			particles.loop = false;
			particles.disableOnDone = true;
			particles.disableOnDoneRoutine = ONDONE.Destroy;
		}
	}
}
