﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game2Manager : Singleton<Game2Manager>
{
    public Transform capsule;
    public Transform cube;
    public Transform sphere;
    public Transform poly;
    public Transform cylinder;

    public List<float> weightRandom;
    float sumWeight;
    public float timeSpawnObj;

    public PoolManager pollManager;
    public MyController controller;
    public float score;

    public int maxScore;
    public int yellowScore;
    public int blueScore;
    public int redScore;
    public float deltaScoreDown;
    public float timePlay;
    float countTimePlay;

    float counter;

    public List<MyCollection2> myCollections;

    public GameState currentState;

    public float minX;
    public float maxX;
    public float startY;
    public static float endY = 6;

    public bool isInitScoreBar;
    // Use this for initialization
    void Start()
    {
        myCollections = new List<MyCollection2>();
        foreach (var item in weightRandom)
        {
            sumWeight += item;
        }
        currentState = GameState.StartGame;
        score = 0;
        countTimePlay = timePlay;
        InitScoreBar();
    }

    public void StartGame()
    {
        currentState = GameState.StartGame;
        score = 0;
        countTimePlay = timePlay;
        UpdateScore();
    }

    public void InitScoreBar()
    {
        if (GuiManager.Instance.ingame1Gui != null)
        {
            GuiManager.Instance.ingame1Gui.SetYellowBlueRedWidthMySlider(yellowScore / (float)maxScore, blueScore / (float)maxScore);
        }
    }

    void OnValidate()
    {
        redScore = maxScore - yellowScore - blueScore;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case GameState.StartGame:
                score = 0;
                currentState = GameState.Ingame;
                break;
            case GameState.Ingame:
                 UpdateTime();
                    UpdateScore();
                counter += Time.deltaTime;
                if (counter >= timeSpawnObj)
                {
                    Vector3 pos = new Vector3(Random.Range(minX, maxX), startY, transform.parent.position.z);
                    float random = Random.Range(0, sumWeight);
                    float count = 0;
                    for (int i = 0; i < weightRandom.Count; i++)
                    {
                        count += weightRandom[i];
                        if (random <= count)
                        {
                            switch (i)
                            {
                                case 0:
                                    MyCollection2 temp = pollManager.Spawn(capsule, pos).GetComponent<MyCollection2>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 1:
                                    temp = pollManager.Spawn(cube, pos).GetComponent<MyCollection2>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 2:
                                    temp = pollManager.Spawn(sphere, pos).GetComponent<MyCollection2>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 3:
                                    temp = pollManager.Spawn(poly, pos).GetComponent<MyCollection2>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                case 4:
                                    temp = pollManager.Spawn(cylinder, pos).GetComponent<MyCollection2>();
                                    temp.Init();
                                    myCollections.Add(temp);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    }
                    counter = 0;
                }
                break;
            case GameState.End:
                break;
            default:
                break;
        }
    }

    public void UpdateTime()
    {
        countTimePlay -= Time.deltaTime;
        if (countTimePlay > 0)
        {
            GuiManager.Instance.ingame1Gui.SetTime((int)countTimePlay);
        }
        else
        {
            if (MenuMng.instance)
            {
                if (score <= yellowScore)
                {
                    //Thua Yellow
                    MenuMng.instance.SetLoseScore(((int)score).ToString(), false);
                    MenuMng.instance.setState((int)MenuDef.menuState.LOSEGAME);
                }
                else if (score <= yellowScore + blueScore)
                {
                    //Thang
                    MenuMng.instance.SetWinScore(((int)score).ToString());
                    MenuMng.instance.setState((int)MenuDef.menuState.WINGAME);
                }
                else
                {
                    //Thua Red
                    MenuMng.instance.SetLoseScore(((int)score).ToString(), true);
                    MenuMng.instance.setState((int)MenuDef.menuState.LOSEGAME);
                }

                currentState = GameState.End;
            }
        }
    }

    public void UpdateScore()
    {
        if (score >= 0 && score <= maxScore)
        {
            score -= deltaScoreDown * Time.deltaTime;
            GuiManager.Instance.ingame1Gui.SetMySliderValue(score / maxScore, (int)score);
        }
    }

    public void Despawn(MyCollection2 myCollection)
    {
        pollManager.Despawn(myCollection.transform);
        myCollections.Remove(myCollection);
    }

    public void Collected(MyCollection2 coll)
    {
        score += coll.score;
        if (score > maxScore)
        {
            score = maxScore;
        }

        SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_GETPOINT);
    }

    public void SubCollected(MyCollection2 coll)
    {
        score -= coll.subScore;
        if (score < 0 )
        {
            score = 0;
        }

        SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_WRONGHIT);
    }

    public void Dead()
    {
		Instantiate (Resources.Load("PlaygroundExplosive", typeof(GameObject)), controller.transform.position, Quaternion.identity);
		controller.currentState = eControllerState.Dying;
    }
}
