﻿using UnityEngine;
using System.Collections;

public class EM_OrbitingCube : MonoBehaviour
{
	Transform transf;
	
	Vector3 rotationVector;
	float rotationSpeed;
	
	Vector3 randomSphereRotation;
	float fadeOutY = 0.0f;
	
	public bool isCollided;
	public Color orginColor;
	public Color targetColor;

	void Start() 
	{
		transf = this.transform;
		rotationVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		rotationVector = Vector3.Normalize(rotationVector);
		
		randomSphereRotation = new Vector3(Random.Range(-1.1f, 1.0f), Random.Range(0f, 0.1f), Random.Range(0.5f, 1f));
		randomSphereRotation = Vector3.Normalize(randomSphereRotation);
		
		rotationSpeed = 90f;
		
		Color orginColor = this.GetComponent<Renderer>().material.color;
	}

	void Update() 
	{
		transf.rotation = Quaternion.Euler(rotationVector * Time.time * rotationSpeed);
		transf.position -= new Vector3 (0.0f, (this.isCollided ? 0.4f : 1f) * Time.deltaTime, 0.0f);

		if (this.isCollided) 
		{
			fadeOutY += Time.deltaTime;
			
			Color updatedColor = orginColor;
			updatedColor.a = 1.0f - fadeOutY;
			updatedColor.r = Mathf.Lerp(orginColor.r, targetColor.r, fadeOutY * 2);
			updatedColor.g = Mathf.Lerp(orginColor.g, targetColor.g, fadeOutY * 2);
			updatedColor.b = Mathf.Lerp(orginColor.b, targetColor.b, fadeOutY * 2);
			this.GetComponent<Renderer>().material.color = updatedColor;
		}
	}
}

