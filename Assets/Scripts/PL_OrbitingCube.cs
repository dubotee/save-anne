﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

public class PL_OrbitingCube : MonoBehaviour
{

	Transform transf;

	Vector3 rotationVector;
	float rotationSpeed;

	Vector3 spherePosition;

	Vector3 randomSphereRotation;

	float sphereRotationSpeed;
	float offsetY = 0.0f;

	Vector3 Vec3(float x)
	{
		return new Vector3(x, x, x);
	}

	void Start() 
	{
		transf = this.transform;
		rotationVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		rotationVector = Vector3.Normalize(rotationVector);

		randomSphereRotation = new Vector3(Random.Range(-1.1f, 1.0f), Random.Range(0f, 0.1f), Random.Range(0.5f, 1f));
		randomSphereRotation = Vector3.Normalize(randomSphereRotation);

		sphereRotationSpeed = 40f;

		rotationSpeed = 90f;
		
		offsetY = Random.Range (200.0f, 100.0f);
	}
	void Update() 
	{
		transf.rotation = Quaternion.Euler(rotationVector * Time.time * rotationSpeed);

		Color color = GetComponent<Renderer>().material.GetColor ("_EmissionColor");
		
		color.r = Mathf.Pow(Mathf.Clamp((Mathf.Sin(Time.time) * 2) / 4, 0, 1 ), 3.0f);

		GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
		GetComponent<Light>().color = color;
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.GetComponent<EM_OrbitingCube>() != null)
		{
			if (!col.GetComponent<EM_OrbitingCube>().isCollided)
			{
				col.GetComponent<EM_OrbitingCube>().isCollided = true;
				col.GetComponent<EM_OrbitingCube>().targetColor = GetComponent<Renderer>().material.GetColor ("_DiffuseColor");

				PlaygroundParticlesC particles = PlaygroundC.Particle();
				particles.gameObject.layer = LayerMask.NameToLayer("Playable");
				particles.source = SOURCEC.WorldObject;
				particles.worldObject.gameObject = col.gameObject;
				particles.loop = false;

				//Manipulator 
				ManipulatorObjectC targetMani = new ManipulatorObjectC();
				particles.manipulators.Add(targetMani);

				targetMani.type = MANIPULATORTYPEC.Property;
				targetMani.transform.SetFromTransform(gameObject.transform);

				targetMani.property.type = MANIPULATORPROPERTYTYPEC.MeshTarget;
				targetMani.property.meshTarget.gameObject = this.gameObject;

				targetMani.property.targetSorting = TARGETSORTINGC.Linear;
				targetMani.property.transition = MANIPULATORPROPERTYTRANSITIONC.Lerp;
				targetMani.shape = MANIPULATORSHAPEC.Sphere;
				targetMani.size = 19.0f;
				targetMani.strength = 5.0f;


			}
		}
	}
}
