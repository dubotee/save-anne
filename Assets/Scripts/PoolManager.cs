﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

    public List<Transform> spawneds;

    public Transform Spawn(Transform tran, Vector3 pos) 
    {
        foreach (var item in spawneds)
        {
			if (!item.gameObject.activeSelf && item.name == tran.name + "(Clone)")
            {
				item.position = item.parent.position + pos;
                item.gameObject.SetActive(true);
                return item;
            }
        }
        GameObject kq = GameObject.Instantiate(tran.gameObject) as GameObject;
        kq.transform.parent = transform;
		kq.transform.position = kq.transform.parent.position + pos;
		//kq.transform.localPosition = pos;
        spawneds.Add(kq.transform);
        return kq.transform;
    }

    public void Despawn(Transform tran)
    {
        foreach (var item in spawneds)
        {
            if (item.gameObject.activeSelf && item == tran)
            {
                item.gameObject.SetActive(false);
                return;
            }
        }
    }
}
