﻿using UnityEngine;
using System.Collections;

public class IDConst
{
    public int ID;
    public string NAME;
    public int TYPE;
    public int FX;

    public IDConst(int id, string name, int type, int fx)
    {
        ID = id;
        NAME = name;
        TYPE = type;
        FX = fx;
    }
}

public class DataConst{

    public static readonly IDConst ID_SOUND_INGAME_BG_MUSIC = new IDConst(0, "Sounds/MenuMusic", -1, -1);
    public static readonly IDConst ID_SOUND_INGAME_BTNCLICK = new IDConst(1, "Sounds/Click", -1, -1);

    public static readonly IDConst ID_SOUND_MM_BG_MAINMENU = new IDConst(2, "Sounds/MainMenu", -1, -1);
    public static readonly IDConst ID_SOUND_MM_BG_STORY = new IDConst(3, "Sounds/Story", -1, -1);
    public static readonly IDConst ID_SOUND_MM_BG_LV1 = new IDConst(4, "Sounds/Level1", -1, -1);
    public static readonly IDConst ID_SOUND_MM_BG_LV2 = new IDConst(5, "Sounds/Level2", -1, -1);
    public static readonly IDConst ID_SOUND_INGAME_GETPOINT = new IDConst(6, "Sounds/Getpoint", -1, -1);
    public static readonly IDConst ID_SOUND_INGAME_WRONGHIT = new IDConst(7, "Sounds/WrongHit", -1, -1);
    public static readonly IDConst ID_SOUND_INGAME_LOSE = new IDConst(8, "Sounds/Losing", -1, -1);
    public static readonly IDConst ID_SOUND_INGAME_WIN = new IDConst(9, "Sounds/Victory", -1, -1);
    public const int ID_SOUNDS_TOTAL = 10;

    public static readonly IDConst[] ID_SOUNDS_LOADING = {
        ID_SOUND_INGAME_BG_MUSIC
        ,ID_SOUND_INGAME_BTNCLICK

        ,ID_SOUND_MM_BG_MAINMENU
        ,ID_SOUND_MM_BG_STORY
        ,ID_SOUND_MM_BG_LV1
        ,ID_SOUND_MM_BG_LV2
        ,ID_SOUND_INGAME_GETPOINT
        ,ID_SOUND_INGAME_WRONGHIT
        ,ID_SOUND_INGAME_LOSE
        ,ID_SOUND_INGAME_WIN
	};
}
