﻿using UnityEngine;
using System.Collections;

public class SoundDef
{
    public const int ID_SOUND_INGAME_BG_MUSIC = 0;
    public const int ID_SOUND_INGAME_BTNCLICK = 1;

    public const int ID_SOUND_MM_BG_MAINMENU = 2;
    public const int ID_SOUND_MM_BG_STORY = 3;
    public const int ID_SOUND_MM_BG_LV1 = 4;
    public const int ID_SOUND_MM_BG_LV2 = 5;
    public const int ID_SOUND_INGAME_GETPOINT = 6;
    public const int ID_SOUND_INGAME_WRONGHIT = 7;
    public const int ID_SOUND_INGAME_LOSE = 8;
    public const int ID_SOUND_INGAME_WIN = 9;
}
