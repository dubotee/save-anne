﻿using UnityEngine;
using System.Collections;

public class FaceAnimVirus : MonoBehaviour {

    public GameObject mySprite;
    public STATE state;
    private float timeCount;
    private float randTime;

	// Use this for initialization
	void Start () {
        state = STATE.SHOW;
        timeCount = 0;
        randTime = Random.Range(2f, 5f);
        UpdateEye(true);
	}
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case STATE.SHOW:
            {
                timeCount += Time.deltaTime;
                if (timeCount >= randTime)
                {
                    timeCount = 0.0f;
                    randTime = Random.Range(0.1f, 0.5f);
                    UpdateEye(false);
                    state = STATE.STAND;
                }
                break;
            }

            case STATE.STAND:
            {
                timeCount += Time.deltaTime;
                if (timeCount >= randTime)
                {
                    timeCount = 0.0f;
                    randTime = Random.Range(2f, 6f);
                    UpdateEye(true);
                    state = STATE.SHOW;
                }
                break;
            }
            default:
                break;
        }
	}

    private void UpdateEye(bool value)
    {
        if (mySprite)
        {
            if (mySprite.activeInHierarchy != value)
            {
                mySprite.SetActive(value);
            }
        }
    }
}
