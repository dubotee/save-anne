﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum STATE
{
    SHOW,
    STAND,
    HIDE
}

public class GameStory : MonoBehaviour {

    public List<Sprite> storyRec;
    public STATE state;
    private int numStory;
    private int currentStory;
    private float timeCount;
    public SpriteRenderer mySpriteRenderer;
    private Color myColor;

    public void StartGameStory()
    {
        state = STATE.SHOW;
        timeCount = 0;
        currentStory = 0;
        numStory = storyRec.Count;
        myColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        mySpriteRenderer.color = myColor;
        //myColor = mySpriteRenderer.color;
        //UpdateStory(storyRec[currentStory]);
    }

    void Update()
    {
        switch (state)
        {
            case STATE.SHOW:
            {
                timeCount += Time.deltaTime;
                myColor.a = timeCount / 3f;
                if (timeCount >= 3f)
                {
                    timeCount = 0.0f;
                    myColor.a = 1.0f;
                    state = STATE.STAND;
                }
                mySpriteRenderer.color = myColor;
                break;
            }

            case STATE.STAND:
            {
                timeCount += Time.deltaTime;
                if (timeCount >= 2.0f)
                {
                    timeCount = 0.0f;
                    state = STATE.HIDE;
                }
                break;
            }

            case STATE.HIDE:
            {
                timeCount += Time.deltaTime;
                myColor.a = 1 - timeCount / 0.5f;
                if (timeCount >= 0.5f)
                {
                    timeCount = 0.0f;
                    Next();
                    state = STATE.SHOW;
                }
                mySpriteRenderer.color = myColor;
                break;
            }
            default:
                break;
        }
    }

    public void Next()
    {
        currentStory++;
        if (currentStory >= numStory)
        {
            MenuMng.instance.GameStoryFinish();
            return;
        }
        UpdateStory(storyRec[currentStory]);
    }

    void OnClick()
    {
        timeCount = 0.0f;
        state = STATE.HIDE;
    }

    private void UpdateStory(Sprite spriteId)
    {
        mySpriteRenderer.sprite = spriteId;
    }
}
