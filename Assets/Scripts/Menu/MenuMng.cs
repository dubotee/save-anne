﻿using UnityEngine;
using System.Collections;

public class MenuMng : MonoBehaviour {
    public static MenuMng instance;

    private int state;
    private int curentState;
    private int issue;
    private bool setDepth;
    private bool isShowStory;

    private int mapId;

    private GameObject MenuManager;

    private GameObject AlphaBlend;
    private GameObject MainMenu;
    private GameObject StartGame;
    private GameObject WomanMap;
    private GameObject Story;
    private GameObject LoseGame;
    private GameObject WinGame;
    private GameObject Infomation;

    private GameObject InGame1;

    public UILabel winText;
    public UILabel winText1;
    public UILabel loseText;
    public UILabel loseText1;

    //DepthManager
    DepthMng depthMng = new DepthMng();

    private GameObject LightBackGround;
    private int numPageInfo;
    private GameObject Page1;
    private GameObject Page2;

    void Start()
    {
        instance = this;

        state = (int)MenuDef.menuState.STORY;
        curentState = state;
        setDepth = false;
        mapId = -1;
        isShowStory = false;
        numPageInfo = 0;

        issue = InitResource(MenuDef.menuManager);
        if (issue == (int)MenuDef.issue.SUCCESSFUL)
        {
            issue = InitMenuObj();
            if (issue == (int)MenuDef.issue.SUCCESSFUL)
            {
                Debug.Log("<color=blue>ss init menuObj ok</color>");
                SetUpButtonID();
                SetUpTextID();
                ShowAllObj(false);
                Debug.Log("---------------");
                //SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_BTNCLICK);
                //SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_MAINMENU], true);

                GameStory story = Story.GetComponent<GameStory>();
                story.StartGameStory();
                SoundManager.instance.StopMusic();
                SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_STORY], true);

                ShowBackGround(false);
            }
            else
            {
                Debug.Log("<color=red>Error init menuObj!</color>");
            }
        }
        else
        {
            Debug.Log("<color=red>Error init resource!</color>");
        }
        //if (state == (int)MenuDef.menuState.ON_GAME)
        //{
        //    if (GameManager.Instance != null && GameManager.Instance.isInitScoreBar == false)
        //    {
        //        GameManager.Instance.InitScoreBar();
        //    }
        //}
        
    }

    void Update()
    {
        if (state != curentState)
        {
            curentState = state;
            onChangeState();
        }
        switch (state)
        {
            case (int)MenuDef.menuState.ON_GAME:
            {
                //if (GameManager.Instance != null && GameManager.Instance.isInitScoreBar == false)
                //{
                //    GameManager.Instance.InitScoreBar();
                //}
                UpdateInGame1();
                break;
            };
            case (int)MenuDef.menuState.MAINMENU:
            {
                UpdateMainMenu();
                break;
            }
            case (int)MenuDef.menuState.WORLDMAP:
            {
                UpdateWorldMap();
                break;
            }
            case (int)MenuDef.menuState.STORY:
            {
                UpdateStory();
                break;
            }
            case (int)MenuDef.menuState.STARTGAME:
            {
                UpdateStartGame();
                break;
            }
            case (int)MenuDef.menuState.LOSEGAME:
            {
                UpdateLoseGame();
                break;
            }
            case (int)MenuDef.menuState.WINGAME:
            {
                UpdateWinGame();
                break;
            }
            case (int)MenuDef.menuState.INFOMATION:
            {
                UpdateInfoGame();
                break;
            }

            default:
            {
                Debug.Log("<color=red>Error update wrong state!</color>");
                break;
            }
        }
    }

    private void onChangeState()
    {
        Debug.Log("onChangeState: " + state);
        setDepth = false;
        switch (state)
        {
            case (int)MenuDef.menuState.ON_GAME:
            {
                if (mapId == 1)
                {
                    SoundManager.instance.StopMusic();
                    SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_LV1], true);
                }
                if (mapId == 2)
                {
                    SoundManager.instance.StopMusic();
                    SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_LV2], true);
                }
                break;
            };
            case (int)MenuDef.menuState.MAINMENU:
            {
                //if(isShowStory)
                {
                    ShowBackGround(true);
                    SoundManager.instance.StopMusic();
                    SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_MAINMENU], true);
                    //isShowStory = false;
                }
                break;
            }
            case (int)MenuDef.menuState.WORLDMAP:
            {
                Color objc;
                UISprite obj1 = WomanMap.transform.FindChild("Info/PlayerMap1").GetComponent<UISprite>();
                UISprite obj2 = WomanMap.transform.FindChild("Info/PlayerMap2").GetComponent<UISprite>();
                objc = obj1.color;
                objc.a = 1.0f;
                obj1.color = objc;

                objc = obj2.color;
                objc.a = 1.0f;
                obj2.color = objc;
                break;
            }
            case (int)MenuDef.menuState.STORY:
            {
                GameStory story = Story.GetComponent<GameStory>();
                story.StartGameStory();
                SoundManager.instance.StopMusic();
                SoundManager.instance.PlayMusic(GameLoop.instance.getMySound()[SoundDef.ID_SOUND_MM_BG_STORY], true);
                break;
            }
            case (int)MenuDef.menuState.STARTGAME:
            {
                StartGameLevel FaceBg = StartGame.GetComponent<StartGameLevel>();
                FaceBg.UpdateMapID(mapId);
                TweenScale tweenScale = StartGame.GetComponent<TweenScale>();
                if (tweenScale)
                {
                    tweenScale.ResetToBeginning();
                    tweenScale.Play();
                }
                break;
            }
            case (int)MenuDef.menuState.LOSEGAME:
            {
                TweenScale tweenScale = LoseGame.GetComponent<TweenScale>();
                if (tweenScale)
                {
                    tweenScale.ResetToBeginning();
                    tweenScale.Play();
                }
                SoundManager.instance.StopMusic();
                SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_LOSE);
                break;
            }
            case (int)MenuDef.menuState.WINGAME:
            {
                TweenScale tweenScale = WinGame.GetComponent<TweenScale>();
                if (tweenScale)
                {
                    tweenScale.ResetToBeginning();
                    tweenScale.Play();
                }
                SoundManager.instance.StopMusic();
                SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_LOSE);
                break;
            }
            default:
            {
                Debug.Log("<color=red>Error onChangeState wrong!</color>");
                break;
            }
        }
    }

    private void UpdateInGame1()
    {
        if (!setDepth)
        {
            HidenMenuObj(WomanMap, false);
            HidenMenuObj(Story, false);
            HidenMenuObj(StartGame, false);
            HidenMenuObj(WinGame, false);
            HidenMenuObj(LoseGame, false);
            HidenMenuObj(AlphaBlend, false);

            HidenMenuObj(InGame1, true);
            setDepth = true;
        }
    }

    private void UpdateMainMenu()
    {
        if (!setDepth)
        {
            HidenMenuObj(WomanMap, false);
            HidenMenuObj(Story, false);
            HidenMenuObj(StartGame, false);
            HidenMenuObj(Infomation, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(AlphaBlend, 100, false);
            HidenMenuObj(MainMenu, true);
            depthMng.setDepth(MainMenu, 200, false);
            setDepth = true;
        }
        
    }

    private void UpdateWorldMap()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);
            HidenMenuObj(StartGame, false);
            HidenMenuObj(WinGame, false);
            HidenMenuObj(LoseGame, false);
            HidenMenuObj(InGame1, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(StartGame, 100, false);
            HidenMenuObj(WomanMap, true);
            depthMng.setDepth(WomanMap, 200, false);
            setDepth = true;
        }
        UISprite obj1 = WomanMap.transform.FindChild("Info/PlayerMap1").GetComponent<UISprite>();
        UISprite obj2 = WomanMap.transform.FindChild("Info/PlayerMap2").GetComponent<UISprite>();
        //StartCoroutine(UpdateWomanMap(obj1, obj2, 2.0f));
        UpdateWomanMap(obj1, obj2, 2.0f);
    }

    private void UpdateStory()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);
            HidenMenuObj(WomanMap, false);
            HidenMenuObj(InGame1, false);

            HidenMenuObj(Story, true);
            depthMng.setDepth(Story, 100, false);
            setDepth = true;
        }
    }

    private void UpdateStartGame()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);
            HidenMenuObj(WomanMap, false);
            HidenMenuObj(Story, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(AlphaBlend, 100, false);
            HidenMenuObj(StartGame, true);
            depthMng.setDepth(StartGame, 200, false);
            setDepth = true;
        }
    }

    private void UpdateLoseGame()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);
            HidenMenuObj(WomanMap, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(AlphaBlend, 100, false);
            HidenMenuObj(LoseGame, true);
            depthMng.setDepth(LoseGame, 200, false);
            setDepth = true;
        }
    }

    private void UpdateWinGame()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);
            HidenMenuObj(WomanMap, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(AlphaBlend, 100, false);
            HidenMenuObj(WinGame, true);
            depthMng.setDepth(WinGame, 200, false);
            setDepth = true;
        }
    }

    private void UpdateInfoGame()
    {
        if (!setDepth)
        {
            HidenMenuObj(MainMenu, false);

            HidenMenuObj(AlphaBlend, true);
            depthMng.setDepth(AlphaBlend, 100, false);
            HidenMenuObj(Infomation, true);
            depthMng.setDepth(Infomation, 200, false);
            setDepth = true;
        }
    }
    public void OnClick(int button_id)
    {
        switch (state)
        {
            case (int)MenuDef.menuState.ON_GAME:
            {
                onClickOnGame(button_id);
                break;
            };
            case (int)MenuDef.menuState.MAINMENU:
            {
                onClickMainMenu(button_id);
                break;
            }
            case (int)MenuDef.menuState.STARTGAME:
            {
                onClickStartGame(button_id);
                break;
            }
            case (int)MenuDef.menuState.WORLDMAP:
            {
                onClickWorldMap(button_id);
                break;
            }
            case (int)MenuDef.menuState.LOSEGAME:
            {
                onClickLoseGame(button_id);
                break;
            }
            case (int)MenuDef.menuState.WINGAME:
            {
                onClickWinGame(button_id);
                break;
            }
            case (int)MenuDef.menuState.INFOMATION:
            {
                onClickInfoGame(button_id);
                break;
            }
            case (int)MenuDef.menuState.STORY:
            {
                onClickStoryGame(button_id);
                break;
            }
            default:
            {
                Debug.Log("<color=red>Error onclick wrong state!</color>");
                break;
            }
        }
    }

    private void onClickOnGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.IGM_HOME:
            {
                Debug.Log("<color=blue>IGM_HOME</color>");
                setState((int)MenuDef.menuState.STORY);
                Application.LoadLevel("demoScene");
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickMainMenu wrong button!</color>");
                break;
            }
        }
    }

    private void onClickMainMenu(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.MM_BEGIN:
            {
                Debug.Log("<color=blue>MM_PLAY</color>");
                setState((int)MenuDef.menuState.WORLDMAP);
                break;
            };
            case (int)MenuDef.buttonId.MM_INFO:
            {
                Debug.Log("<color=blue>MM_INFO</color>");
                setState((int)MenuDef.menuState.INFOMATION);
                break;
            };
            case (int)MenuDef.buttonId.MM_ABOUT:
            {
                Debug.Log("<color=blue>MM_ABOUT</color>");
                setState((int)MenuDef.menuState.STARTGAME);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickMainMenu wrong button!</color>");
                break;
            }
        }
    }

    private void onClickStartGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.SG_BEGIN:
            {
                Debug.Log("<color=blue>SG_BEGIN</color>");
                setState((int)MenuDef.menuState.ON_GAME);
                switch (getMapId())
                {
                    case 1:
                        Application.LoadLevel("GamePlay_01_Final");
                        break;
                    case 2:
                        Application.LoadLevel("GamePlay_02_Final");
                        break;
                    default:
                        break;
                }
                break;
            };
            case (int)MenuDef.buttonId.SG_CANCEL:
            {
                Debug.Log("<color=blue>SG_CANCEL</color>");
                setState((int)MenuDef.menuState.WORLDMAP);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickWorldMap wrong button!</color>");
                break;
            }
        }
    }

    private void onClickWorldMap(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.WM_PLAY:
            {
                Debug.Log("<color=blue>MM_PLAY</color>");
                setState((int)MenuDef.menuState.MAINMENU);
                break;
            };
            case (int)MenuDef.buttonId.WM_MAP1:
            {
                Debug.Log("<color=blue>WM_MAP1</color>");
                mapId = 1;
                setState((int)MenuDef.menuState.STARTGAME);
                break;
            };
            case (int)MenuDef.buttonId.WM_MAP2:
            {
                Debug.Log("<color=blue>WM_MAP2</color>");
                mapId = 2;
                setState((int)MenuDef.menuState.STARTGAME);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickWorldMap wrong button!</color>");
                break;
            }
        }
    }

    private void onClickLoseGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.IGM_RESUME:
            {
                Debug.Log("<color=blue>IGM_RESUME</color>");

                if (getMapId() == 1)
                {
                    GameManager.Instance.StartGame();
                }
                else if (getMapId() == 2)
                {
                    Game2Manager.Instance.StartGame();
                }

                setState((int)MenuDef.menuState.ON_GAME);
                break;
            };
            case (int)MenuDef.buttonId.IGM_NEXT:
            {
                Debug.Log("<color=blue>IGM_NEXT</color>");
                setState((int)MenuDef.menuState.WORLDMAP);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickLoseGame wrong button!</color>");
                break;
            }
        }
    }

    private void onClickWinGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.IGM_RESUME:
            {
                Debug.Log("<color=blue>IGM_RESUME</color>");
                //setState((int)MenuDef.menuState.MAINMENU);
                if (getMapId() == 1)
                {
                    GameManager.Instance.StartGame();
                }
                else if (getMapId() == 2)
                {
                    Game2Manager.Instance.StartGame();
                }

                setState((int)MenuDef.menuState.ON_GAME);
                break;
            };
            case (int)MenuDef.buttonId.IGM_NEXT:
            {
                Debug.Log("<color=blue>IGM_NEXT</color>");
                //setState((int)MenuDef.menuState.STARTGAME);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickWinGame wrong button!</color>");
                break;
            }
        }
    }

    private void onClickInfoGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.INFO_NEXT:
            {
                Debug.Log("<color=blue>INFO_NEXT</color>");
                numPageInfo++;
                if (numPageInfo >= 2)
                {
                    numPageInfo = 0;
                }
                if (numPageInfo == 0)
                {
                    Page1.SetActiveRecursively(true);
                    Page2.SetActiveRecursively(false);
                }
                if (numPageInfo == 1)
                {
                    Page1.SetActiveRecursively(false);
                    Page2.SetActiveRecursively(true);
                }
                //setState((int)MenuDef.menuState.MAINMENU);
                break;
            };
            case (int)MenuDef.buttonId.INFO_CANCEL:
            {
                Debug.Log("<color=blue>INFO_CANCEL</color>");
                setState((int)MenuDef.menuState.MAINMENU);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickInfoGame wrong button!</color>");
                break;
            }
        }
    }

    private void onClickStoryGame(int button_id)
    {
        switch (button_id)
        {
            case (int)MenuDef.buttonId.STORY_SKIP:
            {
                Debug.Log("<color=blue>STORY_SKIP</color>");
                isShowStory = true;
                setState((int)MenuDef.menuState.MAINMENU);
                break;
            };
            default:
            {
                Debug.Log("<color=red>Error onClickInfoGame wrong button!</color>");
                break;
            }
        }
    }

    private int InitMenuObj()
    {
        AlphaBlend = FindChildObj(this.MenuManager, MenuDef.alphaBlend);
        MainMenu = FindChildObj(this.MenuManager, MenuDef.mainMenu);
        StartGame = FindChildObj(this.MenuManager, MenuDef.startGame);
        WomanMap = FindChildObj(this.MenuManager, MenuDef.womanMap);
        Story = FindChildObj(this.MenuManager, MenuDef.story);
        LoseGame = FindChildObj(this.MenuManager, MenuDef.lostGame);
        WinGame = FindChildObj(this.MenuManager, MenuDef.winGame);
        Infomation = FindChildObj(this.MenuManager, MenuDef.infomation);
        if (Infomation)
        {
            Page1 = Infomation.transform.FindChild("Page1").gameObject;
            Page2 = Infomation.transform.FindChild("Page2").gameObject;
        }
        InGame1 = FindChildObj(this.MenuManager, MenuDef.inGame1);

        if (AlphaBlend ||
            MainMenu ||
            StartGame ||
            WomanMap ||
            Story ||
            LoseGame ||
            WinGame ||
            Infomation ||

            InGame1
            )
        {
            return (int)MenuDef.issue.SUCCESSFUL;
        }
        return (int)MenuDef.issue.ERROR;
    }

    private GameObject FindChildObj(GameObject obj, string name)
    {
        Transform tranF = obj.transform.FindChild(name);
        if (tranF)
        {
            return tranF.gameObject;
        }
        else
        {
            Debug.Log("<color=red>Can not find GameObject with name: </color>" + name);
            return null;
        }
    }

    private void ShowAllObj(bool value)
    {
        //HidenMenuObj(AlphaBlend, value);
        HidenMenuObj(MainMenu, value);
        HidenMenuObj(StartGame, value);
        HidenMenuObj(WomanMap, value);
        HidenMenuObj(Story, value);
        HidenMenuObj(LoseGame, value);
        HidenMenuObj(WinGame, value);
        HidenMenuObj(Infomation, value);
        HidenMenuObj(InGame1, value);
    }

    void HidenMenuObj(GameObject Obj, bool active)
    {
        if (Obj.activeInHierarchy != active)
        {
            //Utils.Log("-----: " + Obj.name + " - " + active);
            Obj.SetActive(active);
        }
    }

    private int InitResource(string resource)
    {
        GameObject roottemp = Resources.Load<GameObject>(resource);
        MenuManager = Instantiate(roottemp) as GameObject;
        if (MenuManager != null)
        {
            return (int)MenuDef.issue.SUCCESSFUL;
        }
        return (int)MenuDef.issue.ERROR;
    }

    private void SetUpButtonID()
    {
        SetUpButtonID(MainMenu, "Info/btnBegin", (int)MenuDef.buttonId.MM_BEGIN);
        SetUpButtonID(MainMenu, "Info/btnInfo", (int)MenuDef.buttonId.MM_INFO);
        SetUpButtonID(MainMenu, "Info/btnAbout", (int)MenuDef.buttonId.MM_ABOUT);

        SetUpButtonID(WomanMap, "Info/btnPlay", (int)MenuDef.buttonId.WM_PLAY);
        SetUpButtonID(WomanMap, "Info/PlayerMap2/btnMap1", (int)MenuDef.buttonId.WM_MAP1);
        SetUpButtonID(WomanMap, "Info/PlayerMap2/btnMap2", (int)MenuDef.buttonId.WM_MAP2);

        SetUpButtonID(StartGame, "Info/btnBegin", (int)MenuDef.buttonId.SG_BEGIN);
        SetUpButtonID(StartGame, "Info/btnCancel", (int)MenuDef.buttonId.SG_CANCEL);

        SetUpButtonID(LoseGame, "Info/btnResume", (int)MenuDef.buttonId.IGM_RESUME);
        SetUpButtonID(LoseGame, "Info/btnNext", (int)MenuDef.buttonId.IGM_NEXT);

        SetUpButtonID(WinGame, "Info/btnResume", (int)MenuDef.buttonId.IGM_RESUME);
        SetUpButtonID(WinGame, "Info/btnNext", (int)MenuDef.buttonId.IGM_NEXT);

        SetUpButtonID(Infomation, "Info/btnNext", (int)MenuDef.buttonId.INFO_NEXT);
        SetUpButtonID(Infomation, "Info/btnCancel", (int)MenuDef.buttonId.INFO_CANCEL);

        SetUpButtonID(Story, "Info/btnSkip", (int)MenuDef.buttonId.STORY_SKIP);

        SetUpButtonID(InGame1, "Top/btnHome", (int)MenuDef.buttonId.IGM_HOME);
    }

    private void SetUpButtonID(GameObject Obj, string btnName, int btnId)
    {
        MenuButtonId myButton;
        Transform btnTransform = Obj.transform.FindChild(btnName);
        if (btnTransform)
        {
            myButton = btnTransform.GetComponent<MenuButtonId>();
            myButton.setButtonId(btnId);
        }
        else
        {
            Debug.Log("<color=red>SetUpButtonID wrong with: " + Obj.name + " --- child: " + btnName + "</color>");
        }

    }

    private void SetUpTextID()
    {
        if(WinGame)
        {
            winText = WinGame.transform.FindChild("Info/txtScore").GetComponent<UILabel>();
            winText1 = WinGame.transform.FindChild("Info/txtScore1").GetComponent<UILabel>();
        }
        if (LoseGame)
        {
            loseText = LoseGame.transform.FindChild("Info/txtScore").GetComponent<UILabel>();
            loseText1 = LoseGame.transform.FindChild("Info/txtScore1").GetComponent<UILabel>();
        }
    }

    private void UpdateWomanMap(UISprite obj1, UISprite obj2, float timeCount)
    {
        Color obj1c = obj1.color;
        Color obj2c = obj2.color;
        if (obj1c.a <= 0)
            return;
        obj1c.a -= (Time.deltaTime / 1.5f);
        obj2c.a += (Time.deltaTime / 1.5f);
        //Debug.Log("-- : " + a);
        obj1.color = obj1c;
        obj2.color = obj2c;
    }

    public void GameStoryFinish()
    {
        isShowStory = true;
        setState((int)MenuDef.menuState.MAINMENU);
    }

    public void setState(int value)
    {
        state = value;
    }
    public int getState()
    {
        return state;
    }

    public int getMapId()
    {
        return mapId;
    }

    public int getCurrentState()
    {
        return curentState;
    }

    public void SetWinScore(string text)
    {
        winText.text = text;
    }

    public void SetLoseScore(string text, bool greeter)
    {
        if (greeter)
        {
            loseText1.text = "Over dose!";
            loseText1.color = Color.red;
        }
        else
        {
            loseText1.text = "Under dose!";
            loseText1.color = Color.yellow;
        }
        loseText.text = text;
    }

    private void ShowBackGround(bool value)
    {
        GameObject BackGround = GameObject.Find("Background");
        if (BackGround)
        {
            LightBackGround = BackGround.transform.FindChild("Directionallight").gameObject;
            LightBackGround.SetActive(value);
        }
    }
}
