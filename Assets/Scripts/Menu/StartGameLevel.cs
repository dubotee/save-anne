﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StartGameLevel : MonoBehaviour {
    public UISprite mySprite;
    public GameObject Virus1;
    public GameObject Virus2;

    public void UpdateMapID(int mapId)
    {
        string spriteName = "alpha_location_" + mapId.ToString();
        mySprite.spriteName = spriteName;
        switch (mapId)
        {
            //Cheat map
            case 1: //1
            {
                Debug.Log("StartGameLevel::UpdateMapID: " + mapId);
                Virus1.SetActive(true);
                Virus2.SetActive(false);
                break;
            }
            case 2: //2
            {
                Debug.Log("StartGameLevel::UpdateMapID: " + mapId);
                Virus1.SetActive(false);
                Virus2.SetActive(true);
                break;
            }
            default:
            {
                Debug.Log("<color=red>Error UpdateMapID</color>");
                break;
            }
        }
    }
}
