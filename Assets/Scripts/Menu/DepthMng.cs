﻿using UnityEngine;
using System.Collections;

public class DepthMng : MonoBehaviour {
    // Use this for initialization
    void Start()
    {
        setDepth(this.gameObject, 100, false);
        //setColider(this.gameObject, false, false);
    }

    void Update()
    {
        //setColider(this.gameObject, false, false);
    }

    public void setDepth(GameObject Obj, int value, bool justThisObj)
    {
        UISprite nGuiSprite = Obj.GetComponent<UISprite>();
        UILabel nGuiLabel = Obj.GetComponent<UILabel>();
        if (nGuiSprite)
        {
            nGuiSprite.depth = value;
        }
        if (nGuiLabel)
        {
            nGuiLabel.depth = value;
        }
        if (!justThisObj)
        {
            for (int i = 0; i < Obj.transform.childCount; i++)
            {
                GameObject objChild = Obj.transform.GetChild(i).gameObject;
                setDepth(objChild, value + 1, false);
            }
        }
    }

    public void setColider(GameObject Obj, bool value, bool justThisObj)
    {
        BoxCollider objColider = Obj.GetComponent<BoxCollider>();
        if (objColider && objColider.enabled != value)
        {
            objColider.enabled = value;
        }

        if (!justThisObj)
        {
            for (int i = 0; i < Obj.transform.childCount; i++)
            {
                GameObject objChild = Obj.transform.GetChild(i).gameObject;
                setColider(objChild, value, false);
            }
        }
    }
}
