﻿using UnityEngine;
using System.Collections;

public class InGame1Gui : MonoBehaviour
{
    public MySlider mySlider;
    public UILabel time;

    public void SetMySliderValue(float value, int score)
    {
        mySlider.SetValue(value, score);
    }

    public void SetYellowBlueRedWidthMySlider(float yellowLeght, float blueLeght)
    {
        mySlider.SetYellowBlueRedWidth(yellowLeght, blueLeght);
    }

    public void SetTime(int t)
    {
        int s = t % 60;
        int m = t / 60;
        string kq = "";
        if (m >= 0)
        {
            if (0 <= m && m < 10)
            {
                kq += "0" + m;
            }
            else
            {
                kq += m;
            }
        }
        kq += ":";
        if (s >= 0)
        {
            if (0 <= s && s < 10)
            {
                kq += "0" + s;
            }
            else
            {
                kq += s;
            }
        }
        time.text = kq;
    }
}
