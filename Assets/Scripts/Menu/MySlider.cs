﻿using UnityEngine;
using System.Collections;

public class MySlider : MonoBehaviour {

    public UISprite back;
    public int backLeght;
    public UISprite yellow;
    public UISprite blue;
    public UISprite red;
    public float yellowLeght;
    public float blueLeght;
    public float redLeght;
    [Range(0, 1)]
    public float value;
    public UISprite fore;
    public Transform thump;
    public UILabel textScore;

    void Awake()
    {
        SetValue(0, 0);
    }

    void OnValidate()
    {
        backLeght = 1;
        redLeght = backLeght - yellowLeght - blueLeght;
        yellow.transform.localPosition = back.transform.localPosition;
        yellow.width = (int)(yellowLeght * back.width);
        blue.transform.localPosition = yellow.transform.localPosition + new Vector3(yellowLeght * back.width, 0, 0);
        blue.width = (int)(blueLeght* back.width);
        red.transform.localPosition = blue.transform.localPosition + new Vector3(blueLeght * back.width, 0, 0);
        red.width = (int)(redLeght * back.width);
        fore.transform.localPosition = back.transform.localPosition;
        fore.width = (int)(value * back.width);
        thump.transform.localPosition = back.transform.localPosition + new Vector3(fore.width, 0, 0);
    }

    public void SetYellowBlueRedWidth(float yellowLeght, float blueLeght)
    {
        this.yellowLeght = yellowLeght;
        this.blueLeght = blueLeght;
        OnValidate();
    }

    public void SetValue(float value, int score) 
    {
        this.value = value;
        textScore.text = score.ToString();
        OnValidate();
    }
}
