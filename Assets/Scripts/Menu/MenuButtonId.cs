﻿using UnityEngine;
using System.Collections;

public class MenuButtonId : MonoBehaviour {
    public int ButtonId;
    public virtual void OnClick()
    {
        SoundManager.instance.Play(SoundDef.ID_SOUND_INGAME_BTNCLICK);
        MenuMng.instance.OnClick(ButtonId);
    }

    public void setButtonId(int value)
    {
        ButtonId = value;
    }

    //public int getButtonId()
    //{
    //    return ButtonId;
    //}
}
