﻿using UnityEngine;
using System.Collections;

public class MenuDef{
    public const string menuManager = "Menu/MenuMng";
    public const string alphaBlend = "AlphaBlend";
    public const string mainMenu = "MainMenu";
    public const string startGame = "StartGame";
    public const string womanMap = "WomanMap";
    public const string story = "GameStory";
    public const string lostGame = "LoseGame";
    public const string winGame = "WinGame";
    public const string infomation = "Infomation";
    public const string inGame1 = "InGame1";

    public enum issue
    {
        ERROR = 0,
        SUCCESSFUL
    };

    public enum menuState
    {
        ON_GAME = 0,
        MAINMENU,
        STARTGAME,
        WORLDMAP,
        STORY,
        LOSEGAME,
        WINGAME,
        INFOMATION
    };

    public enum buttonId
    {
        MM_PLAY = 0,
        MM_BEGIN,
        MM_INFO,
        MM_ABOUT,

        WM_PLAY,
        WM_MAP1,
        WM_MAP2,

        SG_BEGIN,
        SG_CANCEL,

        IGM_RESUME,
        IGM_NEXT,
        INFO_NEXT,
        INFO_CANCEL,

        STORY_SKIP,
        IGM_HOME
    };
}
