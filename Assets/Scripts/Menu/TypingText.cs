﻿using UnityEngine;
using System.Collections;

public class TypingText : MonoBehaviour {

    UILabel m_text;
    GameStory m_story;
    string m_string;
    string m_currentstring;
    int count;
    int state;
    bool isstart;
    public GameObject story;
    public const int TYPING_NORMAL = 0;
    public const int TYPING_SPEEDLY = 1;
    public const int TYPING_WAITING = 2;
    public const int TYPING_STOPED = 3;
    public const float TYPING_NORMAL_DELTA_TIME = 0.05f;
    public const float TYPING_SPEEDLY_DELTA_TIME = 0.005f;
    float lasttimetyping;
    float timetonext;
    void Start()
    {
        m_text = gameObject.GetComponent<UILabel>();
        m_story = story.GetComponent<GameStory>();
    }

    public void StartText(string str)
    {
        m_string = str;
        m_currentstring = "";
        isstart = true;
        count = 0;
        state = TYPING_NORMAL;
        lasttimetyping = Time.time;
    }

    void Update()
    {
        if (isstart)
        {
            UpdateTextTyping();
        }
    }

    void UpdateTextTyping()
    {
        if (state == TYPING_STOPED)
            return;
        if (Time.time < lasttimetyping)
            return;

        if (state != TYPING_WAITING)
        {
            m_currentstring += m_string[count];
            m_text.text = m_currentstring;
            count++;
            if (state == TYPING_NORMAL)
            {
                lasttimetyping = Time.time + TYPING_NORMAL_DELTA_TIME;
            }
            if (state == TYPING_SPEEDLY)
            {
                m_text.text = m_string;
                state = TYPING_WAITING;
                timetonext = Time.time + 1.0f;
            }

            if (count >= m_string.Length)
            {
                state = TYPING_WAITING;
                timetonext = Time.time + 1.0f;
            }
        }
        /*
        if (state == TYPING_WAITING) {
            if (Time.time > timetonext) {
                state = TYPING_STOPED;
                m_story.Next();
            }
        }*/
    }

    void OnClick()
    {
        if (!isstart)
            return;
        if (state == TYPING_NORMAL)
        {
            state = TYPING_SPEEDLY;
        }

        if (state == TYPING_WAITING)
        {
            state = TYPING_STOPED;
            m_story.Next();
        }
    }
}
