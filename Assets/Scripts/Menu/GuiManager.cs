﻿using UnityEngine;
using System.Collections;

public class GuiManager : Singleton<GuiManager>
{
    public InGame1Gui ingame1Gui;

    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
}
